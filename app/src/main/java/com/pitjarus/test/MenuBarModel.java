package com.pitjarus.test;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class MenuBarModel {
    private String menuName;
    private Boolean selected;
}
