package com.pitjarus.test;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class ItemModel {
    private String name;
    private String caption;
    private String time;
    private String participant;
    private Integer qty;
}
