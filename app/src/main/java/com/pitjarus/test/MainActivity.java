package com.pitjarus.test;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.pitjarus.test.databinding.ActivityMainBinding;
import com.pitjarus.test.databinding.RowItemBinding;
import com.pitjarus.test.databinding.RowMenubarBinding;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private ArrayList<ItemModel> itemModelList= new ArrayList<>();
    private ArrayList<MenuBarModel> menuBarModelList= new ArrayList<>();
    private SimpleRecyclerAdapter<ItemModel> itemAdapter;
    private SimpleRecyclerAdapter<MenuBarModel> menuBarAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        itemAdapter = new SimpleRecyclerAdapter<>(
                itemModelList,
                R.layout.row_item,
                (holder, item) -> {
                    RowItemBinding binding = (RowItemBinding) holder.getLayoutBinding();
                    binding.setItemModel(item);
                });
        binding.rvItem.setAdapter(itemAdapter);

        menuBarAdapter = new SimpleRecyclerAdapter<>(
                menuBarModelList,
                R.layout.row_menubar,
                (holder, item) -> {
                    RowMenubarBinding binding = (RowMenubarBinding) holder.getLayoutBinding();
                    binding.setMenuBarModel(item);

                    binding.fieldMenubar.setOnClickListener(onCLick -> {
                        if (item.getSelected() == null || !item.getSelected()) {
                            item.setSelected(true);
                            updateCategoryList(item);
                        }
                    });
                });
        binding.rvMenubar.setAdapter(menuBarAdapter);

        insertData();
    }

    private void insertData(){
        itemModelList.add(new ItemModel("Herlambang Raswanto", "Off White x Nike Air Max 270 White Running Shoes", "22:10", "120 People", 10));
        itemModelList.add(new ItemModel("Sujiwo Tejo", "Orange x Nike Air Max 270 White Running Jacket", "15:00", "70 People", 5));
        itemModelList.add(new ItemModel("Siti Zulaikha", "Off White x Nike Air Max Casual Shoes", "10:05", "4 People", 210));

        menuBarModelList.add(new MenuBarModel("Semua", true));
        menuBarModelList.add(new MenuBarModel("Shoes", false));
        menuBarModelList.add(new MenuBarModel("Jacket", false));
        menuBarModelList.add(new MenuBarModel("3G Design", false));
        menuBarModelList.add(new MenuBarModel("Graphic", false));

        itemAdapter.notifyDataSetChanged();
        menuBarAdapter.notifyDataSetChanged();

    }

    private void updateCategoryList(MenuBarModel selectedCategoryModel) {
        int selectedPosition = menuBarModelList.indexOf(selectedCategoryModel);

        resetCategory(selectedCategoryModel);
        menuBarModelList.set(selectedPosition, selectedCategoryModel);
        menuBarAdapter.notifyItemChanged(selectedPosition);
    }

    private void resetCategory(MenuBarModel selectedCategoryModel) {
        for (MenuBarModel model : menuBarModelList) {
            if (model != selectedCategoryModel) {
                int modelPosition = menuBarModelList.indexOf(model);
                model.setSelected(false);
                menuBarModelList.set(modelPosition, model);
            }
        }

        menuBarAdapter.notifyDataSetChanged();
    }
}
